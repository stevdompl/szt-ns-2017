from django import template
from allauth.socialaccount import providers


register = template.Library()


@register.simple_tag
def active_page(request, view_name, template="active"):
    from django.core.urlresolvers import resolve, Resolver404
    if not request:
        return ""
    try:
        return template if resolve(request.path_info).view_name == view_name else ""
    except Resolver404:
        return ""


@register.simple_tag
def get_active_providers():
    socialaccount_providers = providers.registry.get_list()
    active_providers = []
    for provider in socialaccount_providers:
        try:
            app = provider.get_app({})
            active_providers.append(provider)
        except:
            pass

    return active_providers
