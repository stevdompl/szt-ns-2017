from __future__ import unicode_literals
from django.db import models
from django.utils import timezone


# Create your models here.
class Author(models.Model):
    title = models.CharField(max_length=100, blank=True)
    first_name = models.CharField(max_length=50, blank=True)
    middle_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=50)
    slug = models.SlugField()
    birth_date = models.DateField(blank=True)
    death_date = models.DateField(blank=True)

    def __str__(self):
        string = self.last_name

        if self.first_name.strip() and self.middle_name.strip():
            string += ', %s %s' % (self.first_name.strip(), self.middle_name.strip())
        elif self.first_name.strip():
            string += ', ' + self.first_name.strip()
        if self.title.strip():
            string += ', ' + self.title.strip()

        return string.strip(' \t,')

    class Meta:
        ordering = ('last_name', 'first_name', 'middle_name', 'title')


class Book(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=500, blank=True)
    authors = models.ManyToManyField('Author')
    publication_year = models.PositiveIntegerField()
    slug = models.SlugField()
    cover = models.ImageField(upload_to='uploads/')
    rental_days = models.PositiveSmallIntegerField(default=10)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title', '-publication_year')


class Rental(models.Model):
    start = models.DateField(default=timezone.now, editable=False)
    end = models.DateField()
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    book = models.ForeignKey('Book', on_delete=models.CASCADE)
