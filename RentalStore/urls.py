from django.conf.urls import url
from django.views.generic import ListView, RedirectView

from . import views, models


urlpatterns = [
    url(r'^$', views.homepage, name='homepage'),
    url(r'^enter', views.enter, name='enter'),

    url(r'^book$', views.BookListView.as_view(), name='book_list'),
    url(r'^book/(?P<slug>[-\w]+)$', views.BookDetailView.as_view(), name='book_show'),
    url(r'^book/rent/(?P<slug>[-\w]+)$', views.book_rent, name='book_rent'),

    url(r'^my-shelf$', views.RentedBooksView.as_view(), name='my_shelf'),

    url(r'^login$', RedirectView.as_view(pattern_name='auth_login', permanent=True), name='app_login'),
    url(r'^sign-?in$', RedirectView.as_view(pattern_name='auth_login', permanent=True), name='app_sign_in'),
    url(r'^logout$', RedirectView.as_view(pattern_name='auth_logout', permanent=True), name='app_logout'),
    url(r'^sign-?out$', RedirectView.as_view(pattern_name='auth_logout', permanent=True), name='app_sign_out'),
]
