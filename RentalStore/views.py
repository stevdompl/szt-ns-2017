from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils import timezone
from datetime import datetime
from django.contrib import messages
from models import Book, Rental


@login_required()
def homepage(request):
    context = {
        'title': 'Library'
    }
    if request.user.is_staff or request.user.is_superuser:
        return redirect(reverse('admin:index'))

    return render(request, 'RentalStore/homepage.html', context)


@login_required()
def enter(request):
    if request.user.is_staff or request.user.is_superuser:
        return redirect(reverse('admin:index'))

    return redirect('homepage')


@login_required()
def book_rent(request, slug):
    try:
        book = Book.objects.get(slug=slug)
    except:
        book = None

    if book:
        try:
            last_rental = Rental.objects.get(user=request.user, book=book, end__gte=datetime.today())
        except:
            last_rental = None

        if not last_rental:
            rental = Rental(
                book=book,
                user=request.user,
                end=(timezone.now().date() + timezone.timedelta(days=book.rental_days))
            )

            rental.save()
            messages.add_message(request, messages.SUCCESS, 'The requested book is now available for you to read.')
        else:
            messages.add_message(request, messages.WARNING, 'The requested book is already available for you to read.')
    else:
        messages.add_message(request, messages.ERROR, 'Incorrect book. The requested book does not exist.')

    return redirect(reverse('book_show', args=[book.slug]))


class BookListView(ListView):
    model = Book
    paginate_by = 10
    queryset = Book.objects.all()
    context_object_name = 'books'
    template_name = 'RentalStore/book_list.html'

    def get_context_data(self, **kwargs):
        context = super(BookListView, self).get_context_data(**kwargs)
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BookListView, self).dispatch(request, *args, **kwargs)


class BookDetailView(DetailView):
    model = Book
    context_object_name = 'book'
    template_name = 'RentalStore/book_show.html'

    def get_context_data(self, **kwargs):
        context = super(BookDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()

        book = self.get_object()

        try:
            context['rental'] = Rental.objects.get(user=self.request.user, book=book, end__gte=datetime.today())
        except:
            context['rental'] = None

        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BookDetailView, self).dispatch(request, *args, **kwargs)


class RentedBooksView(ListView):
    model = Rental
    paginate_by = 10
    context_object_name = 'rentals'
    template_name = 'RentalStore/my_shelf.html'

    def get_queryset(self):
        return Rental.objects.filter(
            user=self.request.user,
            end__gte=datetime.today()
        ).order_by('book_id', '-end').select_related('book')

    def get_context_data(self, **kwargs):
        context = super(RentedBooksView, self).get_context_data(**kwargs)
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(RentedBooksView, self).dispatch(request, *args, **kwargs)
