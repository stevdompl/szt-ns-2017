from django.contrib import admin
from attachments.admin import AttachmentInlines
from models import Author, Book
from forms import BookForm


# Register your models here.
class AuthorAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("last_name", "first_name", "middle_name")}


class BookAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title", "publication_year",)}
    inlines = (AttachmentInlines,)
    form = BookForm


admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
