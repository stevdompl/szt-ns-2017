from django import forms
from models import Book
from tinymce.widgets import TinyMCE


# Place your forms here
class BookForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30, 'style': 'display:inline-block'}), required=False)

    class Meta:
        model = Book
        exclude = ()
